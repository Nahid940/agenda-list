import React,{useState} from 'react'

import styles from './Tasks.module.css'


const Tasks=(props)=>{


    const clickHandler=(value)=>{
        console.log("clicked")
    }

    const[is_style,setStyle]=useState(true);
    
    const complete=()=>
    {
        setStyle(false)
    }

    return(
        <div className="card">
            <div className="card-body">
                <p id={props.id} className={is_style?'':styles.complete}>{props.task} </p>
            </div>
            <button className="btn btn-danger btn-sm" style={{width:'5%'}} onClick={()=>props.manageTasks(props.task)} > X </button>
            <button className="btn btn-success btn-sm" style={{width:'5%'}} onClick={complete}> ✓</button>
        </div>
    )
}
// onClick={clickHandler(props.task)}
export default Tasks