import React,{useState} from 'react'

const Form =(props)=>
{

    const onSubmit=(event)=>{
        event.preventDefault()
        props.addTodo(props.value)
    }

    const handleChange=(event)=>{
        props.handleChange(event.target.value)
    }

    return (
        <div>
            <form onSubmit={onSubmit}>
                <div className="col md-3">
                    <input type="text" className="form-control" placeholder="Name of task"
                    value={props.value} onChange={handleChange}/>
                </div>
            </form>
        </div>
    )
}
export default Form