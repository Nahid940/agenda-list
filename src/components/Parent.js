import React,{useState,useCallback} from 'react'
import Buttons from './Buttons'
import Decrement from './Decrement'

const Parent=()=>{

    const [numbers,setNumbers]=useState([
        {
            id:1,
            value:2
        },
        {
            id:2,
            value:3
        },
        {
            id:3,
            value:0
        },

        {
            id:4,
            value:30
        }
    ])


    const dynamoClass=(v)=>{

        var className="badge ";

        if(v<=1)
        {
            className+=" badge-danger"
        }else if(v>=2 && v<=4)
        {
            className+=" badge-warning"
        }else if(v>5)
        {
            className+=" badge-success"
        }
        return className
    }

    // const incrementValue=(v)=>{
        
    // }

    const incrementValue=useCallback(
    (v)=>{
        const newArray=[...numbers]
        const index=newArray.indexOf(v)
        newArray[index]=v;
        newArray[index].value++;
        setNumbers(newArray)
        console.log("increment")
    },[])

    // const decrementValue=(v)=>{
    //     const newArray1=[...numbers]
    //     const index=newArray1.indexOf(v)
    //     newArray1[index]=v;
    //     newArray1[index].value--;
    //     setNumbers(newArray1)
    //     console.log("decrement")
    // }

    const decrementValue=useCallback(
        (v)=>{
            const newArray1=[...numbers]
            const index=newArray1.indexOf(v)
            newArray1[index]=v;
            newArray1[index].value--;
            setNumbers(newArray1)
            console.log("decrement")
         },[] )


    return (
        <div>
            {numbers.map(mp=>(
                <li key={mp.id}>
                    <Decrement   id={mp.id} number={mp}   decrementValue={decrementValue}/>
                    <span className={dynamoClass(mp.value)} >{mp.value}</span>
                    <Buttons id={mp.id} number={mp}   incrementValue={incrementValue}/>
                </li>
            ))}
        </div>
    )
}

export default Parent
