import React from 'react'

// const Buttons=(props)=>{

    // const clickHandler=()=>{
    //     props.incrementValue(props.number)
    // }

    // return (
    //     <button className="btn btn-info btn-sm" onClick={clickHandler}>+</button>
    // )
// }

const Buttons=React.memo(
    (props)=>{
        const clickHandler=()=>{
            props.incrementValue(props.number)
        }
        return (
            <button className="btn btn-info btn-sm" onClick={clickHandler}>+</button>
        )
    }
)

export default Buttons