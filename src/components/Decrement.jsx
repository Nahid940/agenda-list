import React from 'react'

const Decrement=(props)=>{
    const clickHandler=()=>{
        props.decrementValue(props.number)
    }
    return (
        <button className="btn btn-warning btn-sm" onClick={clickHandler}>-</button>
    )
}
export default Decrement