import React,{useState} from 'react'
import "mdbreact/dist/css/mdb.css";
import { MDBBtn, MDBInput, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBIcon, MDBBadge, MDBContainer, MDBRow, MDBCol} from "mdbreact";
import Event from './Event'
import { array } from 'prop-types';
const MDB=()=>{

    const [modal,setModal]=useState(false)

    const [events,setEvents]=useState([
        {
            id: 1,
            time: "10:00",
            title: "Breakfast with Simon",
            location: "Lounge Caffe",
            description: "Discuss Q3 targets"
        },
        {
            id: 2,
            time: "10:30",
            title: "Daily Standup Meeting (recurring)",
            location: "Warsaw Spire Office",
            description: "Discuss Q3 targets"

        },
        {
            id: 4,
            time: "12:00",
            title: "Lunch with Timmoty",
            location: "Canteen",
            description:
            "Project evalutation ile declaring a variable and using an is tatement is a fine way to conditionally render a component, sometimes you might want to use a"
        }
    ])
    const onDelete=(id)=>{
        const deletedevent=events.filter(event=>id!=event.id)
        setEvents(deletedevent)
    }
    const toggleModal=()=>{
        if(modal==true)
        {
            setModal(false)
        }else
        {
            setModal(true)
        }
    }
    const [newEvents,setNewEvents]=useState(
        {
            id: Math.random()*4,
            time: "",
            title: "",
            location: "",
            description: ""
        }
    )

    const handleInputChange = inputName => value => {
        const nextValue = value;

        setNewEvents({...newEvents,
            [inputName]: nextValue
        })
    };

    const handleEvent=()=>{
        setEvents([...events,newEvents])
    }


    return (
         <React.Fragment>
            <MDBContainer>
                <MDBRow>
                        <MDBCol md="9" className="mb-r">
                            <h2 className="text-uppercase my-3">Today:</h2>
                            <div id="eventsss">
                                {events.map(event=>(
                                    <Event
                                        key={event.id}
                                        id={event.id}
                                        time={event.time}
                                        title={event.title}
                                        location={event.location}
                                        description={event.description}
                                        onDelete={onDelete}
                                    />
                                ))}

                                <MDBCol xl="6" md="6" className="mx-auto text-center">
                                    <MDBBtn color="info" rounded onClick={toggleModal}>
                                        Add Event
                                    </MDBBtn>
                                </MDBCol>
                            </div>
                        </MDBCol>
                        
                    </MDBRow>
            </MDBContainer>


            <MDBModal isOpen={modal} toggle={toggleModal}>
                    <MDBModalHeader
                        className="text-center"
                        titleClass="w-100 font-weight-bold"
                        toggle={toggleModal}
                        >
                    Add new event
                    </MDBModalHeader>
                    <MDBModalBody>

                        <MDBInput
                            name="time"
                            label="Time"
                            icon="clock"
                            hint="12:30"
                            group
                            type="text"
                            getValue={handleInputChange('time')}
                        />

                        <MDBInput
                        name="title"
                        label="Title"
                        icon="edit"
                        hint="Briefing"
                        group
                        type="text"
                        getValue={handleInputChange('title')}
                        />

                        <MDBInput
                        name="location"
                        label="Location (optional)"
                        icon="map"
                        group
                        type="text"
                        getValue={handleInputChange('location')}
                        />

                        <MDBInput
                        name="description"
                        label="Description (optional)"
                        icon="sticky-note"
                        group
                        type="textarea"
                        getValue={handleInputChange('description')}
                        />

                    </MDBModalBody>

                    <MDBModalFooter className="justify-content-center">
                        <MDBBtn
                            color="info"
                            onClick={() => {
                            toggleModal();
                            handleEvent()
                            }}
                        >
                            Add
                        </MDBBtn>
                    </MDBModalFooter>
            </MDBModal>
        </React.Fragment>
    )
}

export default MDB