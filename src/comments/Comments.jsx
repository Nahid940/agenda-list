import React from 'react'

const Comments=({comment,deleteComment})=>
{
    return(
        <div>
            <div className="card" style={{marginTop:'10px'}}>
                <div className="card-body">
                    <h5 className="card-title" style={{color:'#007bff'}}>Posted by {comment.name}</h5>
                    <p className="card-text">{comment.comment}</p> 
                    <button className="btn btn-danger" onClick={()=>{deleteComment(comment.id)}}>X</button>
                </div>
            </div>
        </div>
    )
}

export default Comments