import React from 'react'
import { Editor } from '@tinymce/tinymce-react';

const Form=({handleInputFiledChange,handleSubmit})=>{
    return (
        <React.Fragment>
            <div className="row">
                <div className="col-md-12">
                    <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="">Name</label>
                        <input type="text" name="name" className="form-control" onChange={handleInputFiledChange}/>
                    </div>
                        <div className="form-group">
                            <label >Post your comment here</label>
                            <textarea name="comment" id="" cols="10" rows="5" onChange={handleInputFiledChange} className='form-control'></textarea>
                            {/* <Editor
                                init={{
                                height: 200,
                                menubar: false,
                                plugins: [
                                    'advlist autolink lists link image charmap print preview anchor',
                                    'searchreplace visualblocks code fullscreen',
                                    'insertdatetime media table paste code help wordcount'
                                ],
                                toolbar:
                                    'undo redo | formatselect | bold italic backcolor | \
                                    alignleft aligncenter alignright alignjustify | \
                                    bullist numlist outdent indent | removeformat | help'
                                }}
                                textareaName='comment'

                                onChange={onTextAreaChnageHandle}
                            /> */}
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </React.Fragment>
    )
}
export default Form