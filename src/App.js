import React,{useState,useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import Parent from './components/Parent'
// import Form from './todo/Form';
import Tasks from './todo/Tasks';
import ParentOfAll from './Context/ParentOfAll';
import UserOfHoc from './Context/UserOfHoc';
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import MDB from './mdb/MDB';
import Form from './comments/Form'
import Comments from './comments/Comments'
import axios from 'axios'

export const MyContext=React.createContext()

function App() {


  const [tasks,setTasks]=useState([])
  const addTodo=(value)=>{

    setTasks([...tasks,value])
  }
  const [value,setValue]=useState('')
  const handleChange=(value)=>{
    setValue(value)
  }

  
  const manageTasks=(v)=>
  {
    const tasks_array=[...tasks]
    tasks_array.splice(tasks_array.indexOf(v),1)
    setTasks(tasks_array)
  }

  const contextValue={
    name:"Nahid",
    primary: '#31B7DA',
    secondary: '#FF7F00'
  }

  const [comments,setCommnets]=useState([])
  const [lastCommentID,setLastCommentID]=useState(0)

  let showcomments=true

  if(comments==null)
  {
    showcomments=false
  }

  const [newArrayOfCommentc,setNewArrayOfComments]=useState({
        id:'',
        name:'',
        comment:''
  })

  const handleInputFiledChange=(e)=>
  {
      setNewArrayOfComments({...newArrayOfCommentc,id:lastCommentID,[e.target.name]:e.target.value})

      // console.log(newArrayOfCommentc)
  }
 
  const handleSubmit=(event)=>{
    event.preventDefault()
    setCommnets([...comments,newArrayOfCommentc])
    postData()
  }

  const postData=()=>{
    axios.post('http://localhost/react/comment.php?post-comment',
    newArrayOfCommentc)
    .then((response) => {
      // console.log(response);
      setLastCommentID(parseInt(response.data.id)+1)
    }, (error) => {
      console.log(error);
    });
  }
  const loadData=()=>{
    axios.get('http://localhost/react/comment.php?get-comment')
    .then((response) => {
      const newArray=response.data.lists;
      setCommnets(newArray)
      //get the last row id
      setLastCommentID( newArray.length>=1?newArray.slice(-1)[0].id:response.data.incrementValue.AUTO_INCREMENT)

    }, (error) => {
      console.log(error);
    });
  }

  useEffect(() => {
    loadData()
  },[]);

  const deleteComment=(id)=>
  {
    // console.log(id)
     setCommnets(comments.filter(cmnts=>cmnts.id!=id))
     axios.post('http://localhost/react/comment.php?delete-comment',{id:id}).then((response)=>{console.log("Data removed")},(error)=>{console.log("error")})
  }


  return (
    <div className="App">
      {/* <Parent/> */}
      {/* <div className="col-md-6">
          <Form addTodo={addTodo} handleChange={handleChange} value={value}/>
          {tasks.map(task=>(
            <Tasks task={task} key={Math.random()*10}  id={Math.random()*10} manageTasks={manageTasks} />
          ))}
      </div> */}

      {/* <MyContext.Provider  value={contextValue}>
          <ParentOfAll/>
          <UserOfHoc/>
      </MyContext.Provider> */}

      {/* <React.Fragment>
          <MDBContainer>
              <MDBRow>
                <MDBCol lg="6">
                    <MDB/>
                </MDBCol>
                <MDBCol lg="6">Right column</MDBCol>
              </MDBRow>
          </MDBContainer>
        </React.Fragment> */}
        
        <div className="col-md-12">
          <div className="row">
            <div className="col-md-6">
                <Form handleSubmit={handleSubmit} handleInputFiledChange={handleInputFiledChange}/>
            </div>

            <div className="col-md-6">
              Comments
                {
                  (!showcomments?"No Comments Posted": comments.map(comment=>(
                    (comment.comment!=null ?<Comments key={Math.random()} comment={comment}  deleteComment={deleteComment}/>:'')
                  )))
                }
            </div>
          </div>

        </div>
      
    </div>
  );
}

export default App;
