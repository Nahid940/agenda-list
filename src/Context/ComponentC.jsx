import React from  'react'
import ComponentD from './ComponentD'

const ComponentC=()=>
{
    return(
        <div>
            <h2>Component C</h2>
            <ComponentD/>
        </div>
    )
}

export default ComponentC