import React from 'react'
import CommonFunction from './CommonFunction'
const UseOfHoc=(props)=>
{

    return (
        <div>
            <p onMouseMove={props.HandleMouseMove}>
                Hello {props.v}
            </p>
        </div>
    )
}

export default CommonFunction(UseOfHoc)