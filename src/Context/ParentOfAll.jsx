import React from  'react'

import ComponentA from './ComponentA'

const ParentOfAll=()=>
{
    return(
        <div>
            <h2>ParentOfAll</h2>
            <ComponentA/>
        </div>
    )
}

export default ParentOfAll