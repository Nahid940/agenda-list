import React from  'react'
import ComponentB from './ComponentsB'

const ComponentA=()=>
{
    return(
        <div>
            <h2>Component A</h2>
            <ComponentB/>
        </div>
    )
}

export default ComponentA