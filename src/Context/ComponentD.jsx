import React,{useContext} from  'react'
import {MyContext} from '../App'

const ComponentD=()=>
{
    const context=useContext(MyContext)
    return(
        <div>
            <h2 style={{color:context.primary}}>Component D {context.name}</h2>
        </div>
    )
}

export default ComponentD