import React,{useContext} from  'react'
import ComponentC from './ComponentC'
import {MyContext} from '../App'
const ComponentB=()=>
{

    const color=useContext(MyContext)
    return(
        <div>
            <h2 style={{color:color.secondary}}>Component B</h2>
            <ComponentC/>
        </div>
    )
}

export default ComponentB